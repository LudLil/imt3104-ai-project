import xml.etree.ElementTree as element_tree
from mrcnn.model import load_image_gt
from mrcnn.model import mold_image
from mrcnn.utils import compute_ap
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np
import os
from PIL import Image


# Gun dataset files are sorted first by their group number (the first number in the file name),
# then by the numbers in the parentheses
def sort_by_parenthesis_number(x):
    group_number, parenthesis_number = x.split('(')
    return group_number, int(parenthesis_number.split(')')[0])


# Function to extract bounding boxes from an annotation file
def extract_boxes(path):
    if not os.path.exists(path):
        return [], 0, 0

    # Loading the XML tree:
    tree = element_tree.parse(path)
    root = tree.getroot()

    # Boxes will contain all the bounding boxes in this XML
    boxes = []

    # Looping through and casting the text to ints:
    for box in root.findall('.//bndbox'):
        x_min = int(box.find('xmin').text)
        y_min = int(box.find('ymin').text)
        x_max = int(box.find('xmax').text)
        y_max = int(box.find('ymax').text)
        coors = [x_min, y_min, x_max, y_max]
        boxes.append(coors)

    # Getting the image dimensions:
    width = int(root.find('.//size/width').text)
    height = int(root.find('.//size/height').text)
    return boxes, width, height


# Calculate the mean average precisions (m_ap) for a dataset model
def evaluate_model(dataset, pred_model, pred_config):
    ap_list = list()

    i = 0
    for image_id in dataset.image_ids:
        i += 1

        # Print progress every 10 images
        if i % 10 == 0:
            print(f'{i}/{len(dataset.image_ids)}')

        # Load image, bounding box, and mask data from an image_id
        image, image_meta, gt_class_id, gt_bbox, gt_mask = \
            load_image_gt(dataset, pred_config, image_id, use_mini_mask=False)

        # Scale pixels so they match with the scaling of the training data
        # How I understand the mold_image(..) function is that it subtracts every pixel in an image with the mean
        # of every pixel in the dataset given. However, I don't quite understand why this is necessary:
        scaled_image = mold_image(image, pred_config)

        # Convert image into one sample
        sample = np.expand_dims(scaled_image, 0)

        # Make predictions
        predictions = pred_model.detect(sample, verbose=0)

        # Get results for the first sample
        results = predictions[0]

        # Compute the average precision for the image and add it to the AP list
        ap, _, _, _ = compute_ap(gt_bbox, gt_class_id, gt_mask, results['rois'], results['class_ids'],
                                 results['scores'], results['masks'])

        ap_list.append(ap)

    # Return mean AP for all images
    m_ap = np.nanmean(ap_list)
    return m_ap


def detect_weapons_in_baggage(baggage_folder_path, pred_config,
                              gun_pred_model, knife_pred_model):
    baggage_file_names = os.listdir(baggage_folder_path)

    plt.figure()
    plt_index = 1
    col = 3
    rows = int(len(baggage_file_names) / 3)

    # Loop through all images in the baggage dataset folder
    for file_name in baggage_file_names:
        plt.axis('off')

        # Lay out images correctly in the plot
        if plt_index <= rows * col:
            plt.subplot(rows, col, plt_index)

        if file_name.endswith('.png'):
            # Open image and convert it to RBG (JPG) NumPy array.
            # Necessary because gun model trained using JPGs,
            # and detecting guns in a PNG would not work.
            baggage_image = Image.open(baggage_folder_path + file_name)
            baggage_image = baggage_image.convert('RGB')
            baggage_image = np.array(baggage_image)

            # Scale image and get one sample as in evaluate_model
            scaled_image = mold_image(baggage_image, pred_config)
            sample = np.expand_dims(scaled_image, 0)

            # Get predictions
            gun_prediction = gun_pred_model.detect(sample, verbose=0)[0]
            knife_prediction = knife_pred_model.detect(sample, verbose=0)[0]

            # Display image and plot bounding boxes of detected weapons in the image
            plt.imshow(baggage_image)
            plot_bounding_boxes(file_name, 'guns', gun_prediction, 'red')
            plot_bounding_boxes(file_name, 'knives', knife_prediction, 'blue')

            plt_index += 1
    plt.show()


def plot_bounding_boxes(baggage_file_name, weapon_type, prediction, color):
    # Plot bounding boxes if a gun is detected
    weapon_detected = False
    if prediction['rois'].size != 0:
        pred_bound_box_axes = plt.gca()

        # Loop through all bounding boxes from the prediction
        i = 0
        for box_coors in prediction['rois']:
            # Weapon is only detected if the prediction has a high confidence score
            if prediction['scores'][i] >= 0.95:
                weapon_detected = True
                y_1, x_1, y_2, x_2 = box_coors
                width, height = x_2 - x_1, y_2 - y_1
                box = Rectangle((x_1, y_1), width, height, fill=False, color=color)
                pred_bound_box_axes.add_patch(box)    # Draw box
                plt.title('Certainty ' + str(prediction['scores'][i]))
            i += 1
        if not weapon_detected:
            print(f'No {weapon_type} detected in {baggage_file_name}')
    else:
        print(f'No {weapon_type} detected in {baggage_file_name}')
