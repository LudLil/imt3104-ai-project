from mrcnn.config import Config

GUN_MODEL_NAME = 'guns_cfg'
KNIFE_MODEL_NAME = 'knife_cfg'


# Configuration class for the gun recognition model
class GunConfig(Config):
    NAME = GUN_MODEL_NAME
    NUM_CLASSES = 1 + 1    # Background class + gun class
    STEPS_PER_EPOCH = 520  # 520 train images

    # Model seemed to perform worse with these parameters
    '''DETECTION_MIN_CONFIDENCE = 0.95
    DETECTION_NMS_THRESHOLD = 0.0

    IMAGE_MIN_DIM = 768
    IMAGE_MAX_DIM = 768

    RPN_ANCHOR_SCALES = (64, 96, 128, 256, 512)
    DETECTION_MAX_INSTANCES = 20

    LEARNING_RATE = 0.1'''


# Configuration class for the knife recognition model
class KnifeConfig(Config):
    NAME = KNIFE_MODEL_NAME
    NUM_CLASSES = 1 + 1    # Background class + gun class
    STEPS_PER_EPOCH = 500  # 500 train images


# Prediction configuration class for the weapon recognition models
class PredictionConfig(Config):
    NAME = GUN_MODEL_NAME
    NUM_CLASSES = 1 + 1
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
