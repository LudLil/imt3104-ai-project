import WeaponDatasetClass as weapon_class
import WeaponConfigClass as weapon_config
import functions as func
from mrcnn.model import MaskRCNN
import os

# Sources:
# Guide to object detection:
# https://machinelearningmastery.com/how-to-train-an-object-detection-model-with-keras/
#
# Knife dataset:
# http://kt.agh.edu.pl/~matiolanski/KnivesImagesDatabase/
#
# Code used to create annotation files for knife images:
# https://github.com/markjay4k/YOLO-series

# Determines whether to train a new model or to use an existing model
train_new_gun_model = False
train_new_knife_model = False

# Determines whether to evaluate the model
evaluate_gun_model = False
evaluate_knife_model = False

# Baggage to inspect for weapons
baggage_to_check = 'B0020/'

# Model weight files
gun_model_file_name = 'gun_model.h5'
knife_model_file_name = 'knife_model.h5'
coco_model_file_name = 'mask_rcnn_coco.h5'  # Starting point for weapon models

dataset_guns_file_path = 'Datasets/Guns/'
dataset_knives_file_path = 'Datasets/Knives/'
dataset_baggages_file_path = 'Datasets/Baggages/'

# Prepare train and test set class objects
gun_train_set = weapon_class.GunDataset()
gun_train_set.load_dataset(dataset_guns_file_path, is_train=True)
gun_train_set.prepare()
print(f'Gun train images: {len(gun_train_set.image_ids)}')

gun_test_set = weapon_class.GunDataset()
gun_test_set.load_dataset(dataset_guns_file_path, is_train=False)
gun_test_set.prepare()
print(f'Gun test images: {len(gun_test_set.image_ids)}')

knife_train_set = weapon_class.KnifeDataset()
knife_train_set.load_dataset(dataset_knives_file_path, is_train=True)
knife_train_set.prepare()
print(f'Knife train images: {len(knife_train_set.image_ids)}')

knife_test_set = weapon_class.KnifeDataset()
knife_test_set.load_dataset(dataset_knives_file_path, is_train=False)
knife_test_set.prepare()
print(f'Knife test images: {len(knife_test_set.image_ids)}')

# Create and train new gun model
if train_new_gun_model and not train_new_knife_model:
    # Prepare gun configuration class object
    gun_config = weapon_config.GunConfig()
    gun_config.display()

    # Creates and trains gun model, using weights
    # from the MS COCO dataset as a starting point:
    gun_model = MaskRCNN(mode='training', model_dir='./', config=gun_config)
    gun_model.load_weights(coco_model_file_name, by_name=True,
                           exclude=['mrcnn_class_logits', 'mrcnn_bbox_fc', 'mrcnn_bbox', 'mrcnn_mask'])
    gun_model.train(gun_train_set, gun_test_set, learning_rate=gun_config.LEARNING_RATE,
                    epochs=10, layers='heads')

    # Saves final model to an h5 file
    gun_model.keras_model.save_weights(gun_model_file_name)
    print(f'Saved gun model to {gun_model_file_name}')

# Create and train new knife model
elif train_new_knife_model and not train_new_gun_model:
    # Prepare knife configuration class object
    knife_config = weapon_config.KnifeConfig()
    knife_config.display()

    # Creates and trains knife model, using weights
    # from the MS COCO dataset as a starting point:
    knife_model = MaskRCNN(mode='training', model_dir='./', config=knife_config)
    knife_model.load_weights(coco_model_file_name, by_name=True,
                             exclude=['mrcnn_class_logits', 'mrcnn_bbox_fc', 'mrcnn_bbox', 'mrcnn_mask'])
    knife_model.train(knife_train_set, knife_test_set, learning_rate=knife_config.LEARNING_RATE,
                      epochs=10, layers='heads')

    # Saves final model to an h5 file
    knife_model.keras_model.save_weights(knife_model_file_name)
    print(f'Saved knife model to {knife_model_file_name}')

# Detect weapons in baggages
elif not train_new_gun_model and not train_new_knife_model:
    if os.path.exists(gun_model_file_name) and os.path.exists(knife_model_file_name):
        # Create prediction config and make prediction model for gun model
        print('Creating gun inference model...')
        pred_config = weapon_config.PredictionConfig()
        gun_pred_model = MaskRCNN(mode='inference', model_dir='./', config=pred_config)
        print('Loading gun model weights...')
        gun_pred_model.load_weights(gun_model_file_name, by_name=True)
        gun_pred_model.keras_model.summary()

        if evaluate_gun_model:
            # Evaluate the gun model on train and test datasets
            print('Evaluating gun model...')
            train_m_ap = func.evaluate_model(gun_train_set, gun_pred_model, pred_config)
            test_m_ap = func.evaluate_model(gun_test_set, gun_pred_model, pred_config)
            print('Train mAP: %.3f' % train_m_ap)
            print('Test mAP: %.3f' % test_m_ap)

        # Create prediction config and make prediction model for knife model
        print('Creating knife inference model...')
        knife_pred_model = MaskRCNN(mode='inference', model_dir='./', config=pred_config)
        print('Loading knife model weights...')
        knife_pred_model.load_weights(knife_model_file_name, by_name=True)
        knife_pred_model.keras_model.summary()
        
        if evaluate_knife_model:
            # Evaluate the knife model on train and test datasets
            print('Evaluating knife model...')
            train_m_ap = func.evaluate_model(knife_train_set, knife_pred_model, pred_config)
            test_m_ap = func.evaluate_model(knife_test_set, knife_pred_model, pred_config)
            print('Train mAP: %.3f' % train_m_ap)
            print('Test mAP: %.3f' % test_m_ap)

        print('Searching for weapons...')
        baggage_folder_path = dataset_baggages_file_path + baggage_to_check
        func.detect_weapons_in_baggage(baggage_folder_path, pred_config,
                                       gun_pred_model, knife_pred_model)
    else:
        print(f'{gun_model_file_name} or {knife_model_file_name} does not exist')
else:
    print('Can only train one model on each run of the program.')
    print('train_new_gun_model and train_new_knife_model cannot both be True.')
