from mrcnn.utils import Dataset
import os
import numpy as np
import functions as func


# Class which loads gun dataset
class GunDataset(Dataset):
    def load_dataset(self, guns_dataset_path, is_train=True):
        self.add_class('dataset', 1, 'gun')

        # Get file names in the gun dataset path, and sort them correctly
        file_names = os.listdir(guns_dataset_path)
        file_names.sort(key=func.sort_by_parenthesis_number)

        # Find all gun images and their XML annotation files
        for file_name in file_names:
            # Only add images, not the XML
            if file_name.split('.')[1] == 'jpg':
                # Find image name with extension removed, which will be used as image ID
                image_name = file_name[:-4]

                # If using the training set, skip over the testing images,
                # and vice versa
                if is_train and file_name.find('t') != -1:
                    continue
                if not is_train and file_name.find('t') == -1:
                    continue

                image_path = guns_dataset_path + file_name
                annotation_path = guns_dataset_path + image_name + '.xml'

                # Add image to dataset
                self.add_image('dataset', image_id=image_name, path=image_path, annotation=annotation_path)

    # Load masks from the image's annotation file
    def load_mask(self, image_id):
        img_info = self.image_info[image_id]
        annotation_path = img_info['annotation']
        boxes, w, h = func.extract_boxes(annotation_path)

        # One array from each mask
        masks = np.zeros([h, w, len(boxes)], dtype='uint8')

        # Create masks
        class_ids = list()
        for i in range(len(boxes)):
            box = boxes[i]
            row_start, row_end = box[1], box[3]
            col_start, col_end = box[0], box[2]
            masks[row_start:row_end, col_start:col_end, i] = 1
            class_ids.append(self.class_names.index('gun'))

        return masks, np.asarray(class_ids, dtype='int32')

    # Get an image reference
    def image_reference(self, image_id):
        img_info = self.image_info[image_id]
        return img_info['path']


# Class which loads knife dataset
class KnifeDataset(Dataset):
    def load_dataset(self, knives_dataset_path, is_train=True):
        self.add_class('knife_dataset', 1, 'knife')

        # Get file names in the knife dataset path
        file_names = os.listdir(knives_dataset_path + '/images')

        # Find all gun images and their XML annotation files
        i = 0
        for file_name in file_names:
            # Add 532 images
            if i < 532:
                i += 1
                # Only add images, not the XML
                if file_name.split('.')[1] == 'bmp':
                    # Find image name with extension removed, which will be used as image ID
                    image_name = 'knife' + file_name[2:-4]
                    annotation_file_name = file_name.split('.')[0] + '.xml'

                    # If using the training set, skip over the testing images,
                    # and vice versa
                    if is_train and file_name.find('t') != -1:
                        continue
                    if not is_train and file_name.find('t') == -1:
                        continue

                    image_path = knives_dataset_path + 'images/' + file_name
                    annotation_path = knives_dataset_path + 'annotations/' + annotation_file_name

                    # Add image to dataset
                    self.add_image('knife_dataset', image_id=image_name, path=image_path, annotation=annotation_path)

    # Load masks from the image's annotation file
    def load_mask(self, image_id):
        img_info = self.image_info[image_id]
        annotation_path = img_info['annotation']
        boxes, w, h = func.extract_boxes(annotation_path)

        # One array from each mask
        masks = np.zeros([h, w, len(boxes)], dtype='uint8')

        # Create masks
        class_ids = list()
        for i in range(len(boxes)):
            box = boxes[i]
            row_start, row_end = box[1], box[3]
            col_start, col_end = box[0], box[2]
            masks[row_start:row_end, col_start:col_end, i] = 1
            class_ids.append(self.class_names.index('knife'))

        return masks, np.asarray(class_ids, dtype='int32')

    # Get an image reference
    def image_reference(self, image_id):
        img_info = self.image_info[image_id]
        return img_info['path']
