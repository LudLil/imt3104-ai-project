# README #

This repository contains a project which uses Keras and Mask R-CNN to detect weapons in X-ray images.

The project was developed using Python in the PyCharm IDE.

We followed this Keras and Mask R-CNN tutorial:
https://machinelearningmastery.com/how-to-train-an-object-detection-model-with-keras/